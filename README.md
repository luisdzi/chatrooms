# Chatrooms

Simple browser-based chat application using .NET, Entity Framework Core, Azure Services Bus and SignalR


## Getting Started


### Prerequisites

* Visual Studio 2019
* .NET Core 2.2 SDK 


### Solution Structure

0.  **App** 
    * ***Chatrooms***: MVC Web App in charge of managing rooms, sending messages between users and commands to the bot.
1.  **Data**
    * ***Chatrooms.Data***: In charge of managing access to the database.
    * ***Chatrooms.Database***: Database project related to Chatrooms App.
    * ***Chatrooms.Domain***: Has all the models related to the database model.
2.  **Business**
    * ***Chatrooms.Dtos***: Contains Dtos objects that links Chatrooms App and the Domain project.
    * ***Chatrooms.Services.Interfaces***: Has all the interfaces related to the services which connect the App and the database. Add a level of abstraction between this two layers.
    * ***Chatrooms.Services.Implementations***: Implementations for the mentioned interfaces above.
3.  **Bot**
    * ***ChatroomsBot***: In charge of processing the commands send by the users in the chatrooms.
4.  **Communication**
    * ***MessageBroker***: Provides an abstraction on how to communicate the bot and the App (send, receive and process messages).
    * ***MessageBroker.AzureServiceBus***: Provides an implementation for the MessageBroker interfaces using AzureServiceBus.
5.  **Test**
    * ***Chatrooms.Tests***: Unit tests for the Chatrooms project.

### Setup

To run the application locally, you need to add the following configuration entries to de secret.json or appsettings.json files


#### Chatrooms

```
  "ConnectionStrings": {
    "ServiceBusConnectionString": "value",
    "Database": "value"
  },
  "ChatroomsBot": {
    "StockQuoutesUrl": "https://chatrooms-bot.azurewebsites.net/api/stockquotes" (or url/api/stockquotes if it is running locally)
  }

```

#### ChatroomsBot

```
"ConnectionStrings": {
    "ServiceBusConnectionString": "value"
  }
}

```

Note: If there is any problem connecting to the database, it is possible that the issue is due to the server's firewall. It is possible to add an exception going to the azure portal and adding the IP as an exception. 
## Deployment

Instead of an installer, I decided to deploy both Web Apps. They have been deployed using Azure App Services.

#### Chatrooms

URL: https://chatroomsapp-0.azurewebsites.net/ (obsolete)

#### ChatroomsBot

URL: https://chatrooms-bot.azurewebsites.net/ (obsolete)


## Libraries

* [Automapper](https://automapper.org/) - A convention-based object-object mapper
* [Stooq](https://github.com/lppkarl/StooqApi/) - Stooq API helper



## Authors

* [**Luis Enrique Dzikiewicz**](https://github.com/luisdzi)







