using Chatrooms.Controllers;
using Chatrooms.Dtos;
using Chatrooms.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatrooms.Tests
{
    [TestClass]
    public class RoomsControllerTests
    {
        [TestMethod]
        public async Task Join_ReturnsNotFound_WhenRoomIdIsNull()
        {
            //Arrange
            var mockRoomService = new Mock<IRoomService>();
            var mockMessageService = new Mock<IMessageService>();

            var controller = new RoomsController(mockRoomService.Object, mockMessageService.Object);

            //Act
            var result = await controller.Join(null);

            //Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task Join_ReturnsNotFound_WhenRoomIdIsNotNull_And_WhenRoomIsNull()
        {
            //Arrange
            var mockRoomService = new Mock<IRoomService>();

            mockRoomService.Setup(m => m.GetRoomById(It.IsAny<int>()))
                .ReturnsAsync((RoomDto)null);

            var mockMessageService = new Mock<IMessageService>();

            var controller = new RoomsController(mockRoomService.Object, mockMessageService.Object);

            //Act
            var result = await controller.Join(1);

            //Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

            mockRoomService.VerifyAll();
        }

        [TestMethod]
        public async Task Join_ReturnsMessages_WhenRoomIdIsNotNull_And_WhenRoomIsNotNull()
        {
            //Arrange
            var mockRoomService = new Mock<IRoomService>();

            mockRoomService.Setup(m => m.GetRoomById(It.IsAny<int>()))
                .ReturnsAsync(GetFakeRoom());

            var mockMessageService = new Mock<IMessageService>();

            mockMessageService.Setup(m => m.GetMessagesByRoomId(It.IsAny<int>()))
                .Returns(GetFakeMessages());

            var controller = new RoomsController(mockRoomService.Object, mockMessageService.Object);

            //Act
            var result = await controller.Join(1);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var resultModel = ((ViewResult)result).Model;

            Assert.IsInstanceOfType(resultModel, typeof(List<MessageDto>));
            Assert.AreEqual("Message 1", ((List<MessageDto>)resultModel).First().Content);

            mockRoomService.VerifyAll();
            mockMessageService.VerifyAll();
        }

        private IEnumerable<MessageDto> GetFakeMessages()
        {
            MessageDto fakeMessage = new MessageDto { Id = 1, Content = "Message 1" };
            return new List<MessageDto> { fakeMessage };
        }

        private RoomDto GetFakeRoom()
        {
            RoomDto fakeRoom = new RoomDto { Id = 1 };
            return fakeRoom;
        }
    }
}
