﻿namespace MessageBroker.Interfaces
{
    public interface IProcessData<T> where T : class
    {
        void Process(T response);
    }
}
