﻿using System.Threading.Tasks;

namespace MessageBroker.Interfaces
{
    public interface IMessageBrokerSender<T> where T : class
    {
        Task SendMessage(T request);
    }
}
