﻿using System.Threading;
using System.Threading.Tasks;

namespace MessageBroker.Interfaces
{
    public interface IMessageBrokerConsumer<TMessage, TResponse> where TMessage : class
    {
        void Initialize();
        Task OnMessageReceiveAsync(TMessage message, CancellationToken cancellationToken);
        Task CloseAsync();
    }
}
