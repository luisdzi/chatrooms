﻿namespace ChatroomsBot.Model
{
    public class StockQuoteBotRequest
    {
        public string StockCode { get; set; }
        public string Username { get; set; }
        public string ConnectionId { get; set; }
    }
}
