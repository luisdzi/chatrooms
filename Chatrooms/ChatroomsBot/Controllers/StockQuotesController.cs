﻿using ChatroomsBot.MessageBroker;
using ChatroomsBot.Model;
using MessageBroker.AzureServiceBus;
using Microsoft.AspNetCore.Mvc;
using StooqApi;
using System;
using System.Threading.Tasks;

namespace ChatroomsBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockQuotesController : ControllerBase
    {
        private readonly StockQueueSender _serviceBusSender;

        public StockQuotesController(StockQueueSender serviceBusSender)
        {
            _serviceBusSender = serviceBusSender;
        }

        [HttpPost]
        public async Task<IActionResult> PostStockQuoteCommand([FromBody]StockQuoteBotRequest stockQuoteRequest)
        {
            string stockCode = stockQuoteRequest.StockCode;
            var candles = await Stooq.GetHistoricalAsync(stockCode, Period.Daily, DateTime.Now.AddDays(-3));

            var stockMessageResponse = new StockQuoteResponse
            {
                StockCode = stockQuoteRequest.StockCode,
                Username = stockQuoteRequest.Username,
                ConnectionId = stockQuoteRequest.ConnectionId,
                Price = candles[0].Open
            };

            // Send this to the bus for the other services
            await _serviceBusSender.SendMessage(stockMessageResponse);

            return Ok(stockQuoteRequest);
        }
    }
}