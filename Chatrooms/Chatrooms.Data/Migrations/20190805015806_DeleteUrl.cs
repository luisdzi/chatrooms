﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatrooms.Data.Migrations
{
    public partial class DeleteUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "Rooms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Rooms",
                nullable: true);
        }
    }
}
