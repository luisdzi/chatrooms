﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Chatrooms.Data.Migrations.Message
{
    public partial class MessageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoomId",
                table: "Message",
                nullable: false,
                defaultValue: 0);


            migrationBuilder.CreateIndex(
                name: "IX_Message_RoomId",
                table: "Message",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_Rooms_RoomId",
                table: "Message",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_Rooms_RoomId",
                table: "Message");


            migrationBuilder.DropIndex(
                name: "IX_Message_RoomId",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "RoomId",
                table: "Message");
        }
    }
}
