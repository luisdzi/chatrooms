﻿using Chatrooms.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chatrooms.Data.Contexts
{
    public class RoomsContext : DbContext
    {
        public RoomsContext(DbContextOptions<RoomsContext> options)
            : base(options)
        {
        }

        public DbSet<Room> Rooms { get; set; }
    }
}
