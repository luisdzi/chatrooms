﻿using Chatrooms.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chatrooms.Data.Contexts
{
    public class MessageContext : DbContext
    {
        public MessageContext(DbContextOptions<MessageContext> options)
            : base(options)
        {
        }

        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>().ToTable("Message");
            modelBuilder.Entity<Room>().ToTable("Rooms");

            modelBuilder.Entity<Message>()
                .HasOne<Room>(ri => ri.Room);
        }
    }
}
