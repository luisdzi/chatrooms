﻿using Chatrooms.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chatrooms.Data.Contexts
{
    public class UsersContext : DbContext
    {
        public UsersContext(DbContextOptions<UsersContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
    }
}
