﻿using Chatrooms.Data.Contexts;
using Chatrooms.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Chatrooms.Data.Seed
{
    public static class DbInitializer
    {
        public static void Initialize(RoomsContext context)
        {
            context.Database.EnsureCreated();

            if (context.Rooms.Any())
            {
                return;
            }

            var room1 = new Room { Title = "Title 1", Description = "Description 1" };
            var room2 = new Room { Title = "Title 2", Description = "Description 2" };
            var room3 = new Room { Title = "Title 3", Description = "Description 3" };
            var room4 = new Room { Title = "Title 4", Description = "Description 4" };
            var room5 = new Room { Title = "Title 5", Description = "Description 5" };
            var room6 = new Room { Title = "Title 6", Description = "Description 6" };
            var rooms = new List<Room>() { room1, room2, room3, room4, room5, room6 };

            context.Rooms.AddRange(rooms);
            context.SaveChanges();
        }
    }
}
