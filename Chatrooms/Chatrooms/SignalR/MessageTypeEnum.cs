﻿namespace Chatrooms.SignalR
{
    public enum MessageTypeEnum
    {
        RegularMessage,
        Command
    }
}
