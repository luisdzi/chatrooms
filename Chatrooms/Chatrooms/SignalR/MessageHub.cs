﻿using Chatrooms.Services.Interfaces;
using ChatroomsBot.Model;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Chatrooms.SignalR
{
    public class MessageHub : Hub
    {
        private readonly IMessageService _messageService;
        private readonly IConfiguration _configuration;

        public MessageHub(IMessageService messageService, IConfiguration configuration)
        {
            _messageService = messageService;
            _configuration = configuration;
        }

        public async Task JoinRoom(int id)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, id.ToString());
        }

        public void SendMessageToGroup(string message, string name, string group)
        {
            switch (GetMessageType(message))
            {
                case MessageTypeEnum.Command:
                    SendStockQuoteRequest(message, name);
                    break;
                default:
                    SendAndSaveRegularMessage(message, name, group);
                    break;
            }
        }

        private void SendStockQuoteRequest(string message, string name)
        {
            string stockCode = new string(message.SkipWhile(c => c != '=').ToArray()).Replace('=', ' ');

            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            using (var client = new HttpClient(clientHandler))
            {
                client.BaseAddress = new Uri(_configuration["ChatroomsBot:StockQuoutesUrl"]);
                StockQuoteBotRequest stockQuoteRequest = new StockQuoteBotRequest { Username = name, StockCode = stockCode, ConnectionId = Context.ConnectionId };

                var postTask = client.PostAsJsonAsync("", stockQuoteRequest).Result;
            }
        }
        private void SendAndSaveRegularMessage(string message, string name, string group)
        {
            DateTime timestamp = DateTime.Now;
            _messageService.CreateRoomMessage(new Dtos.MessageDto { Content = message, Sender = name, Timestamp = timestamp }, Int32.Parse(group));
            Clients.Group(group).SendAsync("SendMessage", name, message, timestamp.ToString("M/d/yyyy h:mm:ss tt"));
        }

        private MessageTypeEnum GetMessageType(string message)
        {
            return message.Contains("/stock") ? MessageTypeEnum.Command : MessageTypeEnum.RegularMessage;
        }
    }
}

