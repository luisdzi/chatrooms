﻿using Chatrooms.Dtos;
using Chatrooms.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatrooms.Controllers
{
    public class RoomsController : Controller
    {
        private readonly IRoomService _roomService;
        private readonly IMessageService _messageService;

        public RoomsController(IRoomService roomService, IMessageService messageService)
        {
            _roomService = roomService;
            _messageService = messageService;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _roomService.GetRooms());
        }

        public async Task<IActionResult> Join(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RoomDto room = await _roomService.GetRoomById(id.Value);
            if (room == null)
            {
                return NotFound();
            }

            ViewBag.RoomName = room.Title;
            ViewBag.RoomDescription = room.Description;

            List<MessageDto> messages = _messageService.GetMessagesByRoomId(id.Value).OrderByDescending(m => m.Timestamp).Take(50).ToList();

            return View("Chat", messages);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description")] RoomDto room)
        {
            if (ModelState.IsValid)
            {
                await _roomService.CreateRoom(room);

                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RoomDto room = await _roomService.GetRoomById(id.Value);
            if (room == null)
            {
                return NotFound();
            }
            return View(room);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description")] RoomDto room)
        {
            if (id != room.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _roomService.UpdateRoom(room);

                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RoomDto room = await _roomService.GetRoomById(id.Value);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _roomService.DeleteRoom(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
