﻿using AutoMapper;
using Chatrooms.Data.Contexts;
using Chatrooms.MessageBroker;
using Chatrooms.Services.Implementations;
using Chatrooms.Services.Implementations.Mapper;
using Chatrooms.Services.Interfaces;
using Chatrooms.SignalR;
using MessageBroker.AzureServiceBus;
using MessageBroker.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chatrooms
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("Database");

            services.AddDbContext<RoomsContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("Chatrooms.Data")));
            services.AddDbContext<UsersContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("Chatrooms.Data")));
            services.AddDbContext<MessageContext>(options => options.UseSqlServer(connection, b => b.MigrationsAssembly("Chatrooms.Data")));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddTransient<IProcessData<StockQuoteResponse>, StockQuotesProcessData>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddSingleton<IStockQueueConsumer, StockQueueConsumer>();

            IMapper mapper = MapperService.BuildMapper();

            services.AddSingleton(mapper);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSignalR(routes =>
            {
                routes.MapHub<MessageHub>("/chat");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            var bus = app.ApplicationServices.GetService<IStockQueueConsumer>();
            bus.Initialize();
        }
    }
}
