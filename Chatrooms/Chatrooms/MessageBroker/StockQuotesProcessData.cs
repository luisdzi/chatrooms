﻿using Chatrooms.SignalR;
using MessageBroker.AzureServiceBus;
using MessageBroker.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;

namespace Chatrooms.MessageBroker
{
    public class StockQuotesProcessData : IProcessData<StockQuoteResponse>
    {
        private readonly IHubContext<MessageHub> _hubContext;

        public StockQuotesProcessData(IHubContext<MessageHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async void Process(StockQuoteResponse stockQuoteResponse)
        {
            await _hubContext.Clients.Client(stockQuoteResponse.ConnectionId).SendAsync("SendMessage", "commandbot", $"{stockQuoteResponse.StockCode.ToUpperInvariant()} quote is ${stockQuoteResponse.Price} per share", DateTime.Now.ToString("M/d/yyyy h:mm:ss tt"));
        }

    }
}
