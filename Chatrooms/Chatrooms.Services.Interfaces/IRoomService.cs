﻿using Chatrooms.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Chatrooms.Services.Interfaces
{
    public interface IRoomService
    {
        Task<RoomDto> GetRoomById(int id);

        Task<IList<RoomDto>> GetRooms();

        Task CreateRoom(RoomDto roomDto);

        Task UpdateRoom(RoomDto roomDto);

        Task DeleteRoom(int id);
    }
}
