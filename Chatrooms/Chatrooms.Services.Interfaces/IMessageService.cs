﻿using Chatrooms.Dtos;
using System.Collections.Generic;

namespace Chatrooms.Services.Interfaces
{
    public interface IMessageService
    {
        IEnumerable<MessageDto> GetMessagesByRoomId(int roomId);

        void CreateRoomMessage(MessageDto messageDto, int roomId);
    }
}
