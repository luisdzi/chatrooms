﻿using AutoMapper;
using Chatrooms.Data.Contexts;
using Chatrooms.Domain.Entities;
using Chatrooms.Dtos;
using Chatrooms.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Chatrooms.Services.Implementations
{
    public class RoomService : Service, IRoomService
    {
        private readonly RoomsContext _roomContext;
        private readonly IMapper _mapper;

        public RoomService(RoomsContext context, IMapper mapper)
        {
            _roomContext = context;
            _mapper = mapper;
        }

        public async Task<RoomDto> GetRoomById(int id)
        {
            return _mapper.Map<Room, RoomDto>(await _roomContext.Rooms
                .FirstOrDefaultAsync(m => m.Id == id));
        }

        public async Task<IList<RoomDto>> GetRooms()
        {
            return _mapper.Map<IList<Room>, IList<RoomDto>>(await _roomContext.Rooms.AsNoTracking().ToListAsync());
        }

        public async Task CreateRoom(RoomDto roomDto)
        {
            Room room = _mapper.Map<RoomDto, Room>(roomDto);

            _roomContext.Add(room);
            await _roomContext.SaveChangesAsync();
        }

        public async Task UpdateRoom(RoomDto roomDto)
        {
            Room room = _mapper.Map<RoomDto, Room>(roomDto);

            _roomContext.Update(room);
            await _roomContext.SaveChangesAsync();
        }

        public async Task DeleteRoom(int id)
        {
            Room room = await _roomContext.Rooms.FindAsync(id);
            _roomContext.Rooms.Remove(room);
            await _roomContext.SaveChangesAsync();
        }
    }
}
