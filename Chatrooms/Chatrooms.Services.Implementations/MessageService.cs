﻿using AutoMapper;
using Chatrooms.Data.Contexts;
using Chatrooms.Domain.Entities;
using Chatrooms.Dtos;
using Chatrooms.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Chatrooms.Services.Implementations
{
    public class MessageService : Service, IMessageService
    {
        private readonly MessageContext _messageContext;
        private readonly IMapper _mapper;

        public MessageService(MessageContext context, IMapper mapper)
        {
            _messageContext = context;
            _mapper = mapper;
        }

        public IEnumerable<MessageDto> GetMessagesByRoomId(int roomId)
        {
            return _mapper.Map<IEnumerable<Message>, IEnumerable<MessageDto>>(_messageContext.Messages.AsNoTracking().Where(m => m.RoomId == roomId));
        }

        public void CreateRoomMessage(MessageDto messageDto, int roomId)
        {
            Message message = _mapper.Map<MessageDto, Message>(messageDto);
            message.RoomId = roomId;

            _messageContext.Add(message);
            _messageContext.SaveChanges();
        }
    }
}
