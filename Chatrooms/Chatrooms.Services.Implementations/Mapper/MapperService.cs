﻿using AutoMapper;

namespace Chatrooms.Services.Implementations.Mapper
{
    public static class MapperService
    {
        public static IMapper BuildMapper()
        {
            MapperConfiguration mappingConfig = CreateMapperConfiguration();

            IMapper mapper = mappingConfig.CreateMapper();
            return mapper;
        }

        private static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(mc =>
            {
                mc.AddProfile(new RoomProfile());
                mc.AddProfile(new MessageProfile());
            });
        }
    }
}
