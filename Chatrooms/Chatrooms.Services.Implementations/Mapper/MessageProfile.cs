﻿using AutoMapper;
using Chatrooms.Domain.Entities;
using Chatrooms.Dtos;

namespace Chatrooms.Services.Implementations.Mapper
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            CreateMap<Message, MessageDto>().ReverseMap();
        }
    }
}
