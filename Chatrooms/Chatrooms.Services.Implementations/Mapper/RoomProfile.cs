﻿using AutoMapper;
using Chatrooms.Domain.Entities;
using Chatrooms.Dtos;

namespace Chatrooms.Services.Implementations.Mapper
{
    public class RoomProfile : Profile
    {
        public RoomProfile()
        {
            CreateMap<Room, RoomDto>().ReverseMap();
        }
    }
}
