﻿using MessageBroker.Interfaces;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageBroker.AzureServiceBus
{
    public interface IStockQueueConsumer : IMessageBrokerConsumer<Message, StockQuoteResponse>
    {
    }

    public class StockQueueConsumer : IStockQueueConsumer
    {
        private readonly IProcessData<StockQuoteResponse> _processData;
        private readonly IConfiguration _configuration;
        private readonly QueueClient _queueClient;
        private const string QUEUE_NAME = "stock-quotes-queue";
        private readonly ILogger _logger;

        public StockQueueConsumer(IProcessData<StockQuoteResponse> processData,
            IConfiguration configuration,
            ILogger<StockQueueConsumer> logger)
        {
            _processData = processData;
            _configuration = configuration;
            _logger = logger;
            _queueClient = new QueueClient(
              _configuration.GetConnectionString("ServiceBusConnectionString"), QUEUE_NAME);
        }

        public void Initialize()
        {
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };

            _queueClient.RegisterMessageHandler(OnMessageReceiveAsync, messageHandlerOptions);
        }

        public async Task OnMessageReceiveAsync(Message message, CancellationToken token)
        {
            var stockQuoteResponse = JsonConvert.DeserializeObject<StockQuoteResponse>(Encoding.UTF8.GetString(message.Body));
            _processData.Process(stockQuoteResponse);
            await _queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            _logger.LogError(exceptionReceivedEventArgs.Exception, "Message handler encountered an exception");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;

            _logger.LogDebug($"- Endpoint: {context.Endpoint}");
            _logger.LogDebug($"- Entity Path: {context.EntityPath}");
            _logger.LogDebug($"- Executing Action: {context.Action}");

            return Task.CompletedTask;
        }

        public async Task CloseAsync()
        {
            await _queueClient.CloseAsync();
        }

    }
}
