﻿using MessageBroker.AzureServiceBus;
using MessageBroker.Interfaces;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace ChatroomsBot.MessageBroker
{
    public class StockQueueSender : IMessageBrokerSender<StockQuoteResponse>
    {
        private readonly QueueClient _queueClient;
        private readonly IConfiguration _configuration;
        private const string QUEUE_NAME = "stock-quotes-queue";

        public StockQueueSender(IConfiguration configuration)
        {
            _configuration = configuration;
            _queueClient = new QueueClient(
              _configuration.GetConnectionString("ServiceBusConnectionString"),
              QUEUE_NAME);
        }

        public async Task SendMessage(StockQuoteResponse payload)
        {
            string data = JsonConvert.SerializeObject(payload);
            Message message = new Message(Encoding.UTF8.GetBytes(data));

            await _queueClient.SendAsync(message);
        }
    }
}
