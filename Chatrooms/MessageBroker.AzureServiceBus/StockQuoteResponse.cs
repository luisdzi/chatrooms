﻿namespace MessageBroker.AzureServiceBus
{
    public class StockQuoteResponse
    {
        public string StockCode { get; set; }
        public string Username { get; set; }
        public string ConnectionId { get; set; }
        public decimal Price { get; set; }
    }
}
