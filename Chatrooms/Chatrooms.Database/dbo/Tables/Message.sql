﻿CREATE TABLE [dbo].[Message] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Content]   NVARCHAR (MAX) NULL,
    [Sender]    NVARCHAR (MAX) NULL,
    [Timestamp] DATETIME2 (7)  DEFAULT ('0001-01-01T00:00:00.0000000') NOT NULL,
    [RoomId]    INT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Message_Rooms_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[Rooms] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_Message_RoomId]
    ON [dbo].[Message]([RoomId] ASC);

